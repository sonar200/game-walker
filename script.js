/**
 * Игра ходилка по мотивам задумки от Регата
 *
 * @author Ivan Ivanov <ivan@sonar200.ru>
 * @version 0.8-b
 */
$(function () {
    /**
     * Значения кубика.
     *
     * @type {array}
     */
    const cube = [1, 2, 3, 4, 5, 6];

    /**
     * Размер ячейки игрового поля
     *
     * @type {number}
     */
    const sizeFieldItem = 40;

    /**
     * Минимальный размер игрового поля
     *
     * @type {number}
     */
    const minSizeField = 8;

    /**
     * Максимальный размер игрового поля
     *
     * @type {number}
     */
    const maxSizeField = 15;

    /**
     * Внешние отступы фишек от границ поля
     * чем больше значение, тем меньше фишки
     *
     * @type {number}
     */
    const marginChip = 4;

    /**
     * Список игроков
     *
     * @type {Array}
     */
    let players = [1];

    /**
     * Список игроков завершивших игру
     *
     * @type {Array}
     */
    let finishedGame = [];

    /**
     * Номер текущего игрока
     * берётся значние из players
     * @see players
     *
     * @type {number}
     */
    let currentPlayer = 1;

    /**
     * Стартовая позиция
     *
     * @type {Array}
     *
     * @prop [0] - Позиция по вертикали
     * @prop [1] - Позиция по горизонтали
     * */
    let startPos = [];

    /**
     * Игровое поле в виде массива
     *
     * @type {Array}
     */
    let gameField = [];

    /**
     * Список последних позиций игроков
     *
     * @type {Array}
     * @property номер_игрока - содержит координаты последней позиции вида [y, x]
     */
    let prevPos = [];

    /**
     * Индикатор цикла шагов для хода
     *
     * @type {number}
     */
    let loopSteps = 0;

    /**
     * Блок игрового поля
     *
     * @type {jQuery.fn.init|jQuery|HTMLElement}
     */
    let gameFieldBlock = $(document).find('.game_field');

    /**
     * Кнопка начала игры
     *
     * @type {jQuery.fn.init|jQuery|HTMLElement}
     */
    let btnStartGame = $('.startGame');

    /**
     * Кнопка хода
     *
     * @type {jQuery.fn.init|jQuery|HTMLElement}
     */
    let btnMakeMovePlayer = $('.makeMovePlayer');

    /**
     * Кнопка добавление игрока
     *
     * @type {jQuery.fn.init|jQuery|HTMLElement}
     */
    let btnAddPlayer = $('.addPlayer');

    /**
     * Поле для отображения сгенерированного значения количества шагов на ход
     *
     * @type {jQuery.fn.init|jQuery|HTMLElement}
     */
    let fieldNumSteps = $('.numSteps');

    /**
     * Блок отображения результатов
     *
     * @type {jQuery.fn.init|jQuery|HTMLElement}
     */
    let blockResult = $('.result').find('tbody');


    /**
     * Проверка занята ли переданная позиция и следующая над ней
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция позиция по вертикали
     * @param {number} positionX Позиция позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyNextTopItem(field, positionY, positionX) {
        let isEmptyThisItem = field[positionY][positionX] === 0;
        let isEmptyNextTopItem = checkTopOrLeftEdge(positionY) || checkEmptyTopItem(field, positionY, positionX);

        return isEmptyThisItem && isEmptyNextTopItem;
    }


    /**
     * Проверка занята ли переданная позиция и следующая под ней
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция позиция по вертикали
     * @param {number} positionX Позиция позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyNextBottomItem(field, positionY, positionX) {
        let isEmptyThisItem = field[positionY][positionX] === 0;
        let isEmptyNextBottomItem = checkRightOrBottomEdge(positionY, field.length) || checkEmptyBottomItem(field, positionY, positionX);

        return isEmptyThisItem && isEmptyNextBottomItem;
    }


    /**
     * Проверка занята ли переданная позиция и следующая слева за ней
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция позиция по вертикали
     * @param {number} positionX Позиция позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyNextLeftItem(field, positionY, positionX) {
        let isEmptyThisItem = field[positionY][positionX] === 0;
        let isEmptyNextLeftItem = checkTopOrLeftEdge(positionX) || checkEmptyLeftItem(field, positionY, positionX);

        return isEmptyThisItem && isEmptyNextLeftItem;
    }


    /**
     * Проверка занята ли переданная позиция и следующая справа за ней
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция позиция по вертикали
     * @param {number} positionX Позиция позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyNextRightItem(field, positionY, positionX) {
        let isEmptyThisItem = field[positionY][positionX] === 0;
        let isEmptyNextRigthItem = checkRightOrBottomEdge(positionX, field.length) || checkEmptyRightItem(field, positionY, positionX);

        return isEmptyThisItem && isEmptyNextRigthItem;
    }


    /**
     * Проверка занят ли элемент над переданной позицией
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция позиция по вертикали
     * @param {number} positionX Позиция позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyTopItem(field, positionY, positionX) {
        return positionY > 0 && field[positionY - 1][positionX] === 0;
    }


    /**
     * Проверка занят ли элемент под переданной позицией
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция позиция по вертикали
     * @param {number} positionX Позиция позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyBottomItem(field, positionY, positionX) {
        return positionY < (field.length - 1) && field[positionY + 1][positionX] === 0;
    }


    /**
     * Проверка занят ли элемент слева от переданной позицией
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция по вертикали
     * @param {number} positionX Позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyLeftItem(field, positionY, positionX) {
        return positionX > 0 && field[positionY][positionX - 1] === 0;
    }


    /**
     * Проверка занят ли элемент справа от переданной позицией
     *
     * @param {Array} field Игровое поле
     * @param {number} positionY Позиция по вертикали
     * @param {number} positionX Позиция по горизонтали
     *
     * @return {boolean}
     */
    function checkEmptyRightItem(field, positionY, positionX) {
        return positionX < (field.length - 1) && field[positionY][positionX + 1] === 0;
    }


    /**
     * Проверка является ли позиция границей слева или сверху
     *
     * @param {number} position Проверяемая позиция по вертикали или горизонтали
     *
     * @return {boolean}
     */
    function checkTopOrLeftEdge(position) {
        return position === 0;
    }


    /**
     * Проверка является ли позиция границей справа или снизу
     *
     * @param {number} position Проверяемая позиция по вертикали или горизонтали
     * @param {number} sizeField Размер игрового поля
     *
     * @return {boolean}
     */
    function checkRightOrBottomEdge(position, sizeField) {
        return position === sizeField - 1;
    }

    /**
     * Построение отрезка дорожки по заданому направлению и количеству шагов
     *
     * @param {Array} field Игровое поле
     * @param {Array} position Стартовая позиция построения отрезка в виде массива [y, x]
     * @param {number} direction Направление движения (0 - вверх, 1 - вниз, 2 - влево, 3 вправо)
     * @param {number} steps Количество шагов на которое строится дорожка
     *
     * @returns {Array|boolean} Возвращает модифизированное игровое поле, если построить отрезок не удалось, то false
     */
    function fillDirectionRoad(field, position, direction, steps) {
        let newFieldData = false;
        if (direction === 0 || direction === 1) {
            newFieldData = fillDirectionRoadVertical(field, position, direction, steps);

        } else if (direction === 2 || direction === 3) {
            newFieldData = fillDirectionRoadHorizontal(field, position, direction, steps);
        }

        return newFieldData;
    }


    /**
     * Построение отрезка дорожки по заданому направлению по вертикали
     *
     * @param {Array} field Игровое поле
     * @param {Array} position Стартовая позиция построения отрезка (0 - позиция по вертикали, 1 - позиция по вертикали)
     * @param {number} direction Направление движения (0 - вверх, 1 - вниз)
     * @param {number} steps Количество шагов на которое строится дорожка
     *
     * @returns {{field: Array, position: Array}|boolean} Возвращает модифизированное игровое поле
     *                                                      и координаты последней ячейки в виде массива [y, x],
     *                                                      если построить отрезок не удалось, то false
     */
    function fillDirectionRoadVertical(field, position, direction, steps) {
        let isRoad = true;
        for (let y = direction === 0 ? position[0] - 1 : position[0] + 1; steps > 0; direction === 0 ? y-- : y++ , steps--) {

            let isEmptyNextItem = direction === 0 ? checkEmptyNextTopItem(field, y, position[1]) : checkEmptyNextBottomItem(field, y, position[1]);
            let isEmptyLeftItem = checkTopOrLeftEdge(position[1]) || checkEmptyLeftItem(field, y, position[1]);
            let isEmptyRightItem = checkRightOrBottomEdge(position[1], field.length) || checkEmptyRightItem(field, y, position[1]);

            if (isEmptyNextItem && isEmptyLeftItem && isEmptyRightItem) {
                field[y][position[1]] = field[y][position[1]] !== 2 ? 1 : 2;
                position[0] = y;
            } else {
                isRoad = false;
                break;
            }
        }

        return isRoad ? {field: field, position: position} : false;
    }


    /**
     * Построение отрезка дорожки по заданому направлению по горизонтали
     *
     * @param {Array} field Игровое поле
     * @param {Array} position Стартовая позиция построения (0 - позиция по вертикали, 1 - позиция по вертикали)
     * @param {number} direction Направление движения (2 - влево, 3 вправо)
     * @param {number} steps Количество шагов на которое строится дорожка
     *
     * @returns {{field: Array, position: Array}|boolean} Возвращает модифизированное игровое поле
     *                                                      и координаты последней ячейки в виде массива [y, x],
     *                                                      если построить отрезок не удалось, то false
     */
    function fillDirectionRoadHorizontal(field, position, direction, steps) {
        let isRoad = true;
        for (let x = direction === 2 ? position[1] - 1 : position[1] + 1; steps > 0; direction === 2 ? x-- : x++ , steps--) {

            let isEmptyNextItem = direction === 2 ? checkEmptyNextLeftItem(field, position[0], x) : checkEmptyNextRightItem(field, position[0], x);
            let isEmptyTopItem = checkTopOrLeftEdge(position[0]) || checkEmptyTopItem(field, position[0], x);
            let isEmptyBottomItem = checkRightOrBottomEdge(position[0], field.length) || checkEmptyBottomItem(field, position[0], x);

            if (isEmptyNextItem && isEmptyTopItem && isEmptyBottomItem) {
                field[position[0]][x] = field[position[0]][x] !== 2 ? 1 : 2;
                position[1] = x;
            } else {
                isRoad = false;
                break;
            }
        }

        return isRoad ? {field: field, position: position} : false;
    }

    /**
     * Проверка можно ли построить дорожку вверх
     *
     * @param {Array} field Игровое поле
     * @param {Array} position Текущая позиция в виде массива [y, x]
     *
     * @return {boolean}
     */
    function isCanCreateDirectionToTop(field, position) {
        let isNotEdge = false === checkTopOrLeftEdge(position[0]);
        let isEmptyTopItem = isNotEdge && checkEmptyTopItem(field, position[0], position[1]);
        let isEmptyNextTopItem = isNotEdge && checkEmptyNextTopItem(field, position[0] - 1, position[1]);
        let isEmptyNextLeftItem = isNotEdge && checkEmptyNextLeftItem(field, position[0] - 1, position[1]);
        let isEmptyNextRightItem = isNotEdge && checkEmptyNextRightItem(field, position[0] - 1, position[1]);

        return isEmptyTopItem && isEmptyNextTopItem && isEmptyNextLeftItem && isEmptyNextRightItem;
    }


    /**
     * Проверка можно ли построить дорожку вниз
     *
     * @param {Array} field Игровое поле
     * @param {Array} position Текущая позиция в виде массива [y, x]
     *
     * @return {boolean}
     */
    function isCanCreateDirectionToBottom(field, position) {
        let isNotEdge = false === checkRightOrBottomEdge(position[0], field.length);
        let isEmptyBottomItem = isNotEdge && checkEmptyBottomItem(field, position[0], position[1]);
        let isEmptyNextBottomItem = isNotEdge && checkEmptyNextBottomItem(field, position[0] + 1, position[1]);
        let isEmptyNextLeftItem = isNotEdge && checkEmptyNextLeftItem(field, position[0] + 1, position[1]);
        let isEmptyNextRightItem = isNotEdge && checkEmptyNextRightItem(field, position[0] + 1, position[1]);

        return isEmptyBottomItem && isEmptyNextBottomItem && isEmptyNextLeftItem && isEmptyNextRightItem;
    }


    /**
     * Проверка можно ли построить дорожку влево
     *
     * @param {Array} field Игровое поле
     * @param {Array} position Текущая позиция в виде массива [y, x]
     *
     * @return {boolean}
     */
    function isCanCreateDirectionToLeft(field, position) {
        let isNotEdge = false === checkTopOrLeftEdge(position[1]);
        let isEmptyLeftItem = isNotEdge && checkEmptyLeftItem(field, position[0], position[1]);
        let isEmptyNextLeftItem = isNotEdge && checkEmptyNextLeftItem(field, position[0], position[1] - 1);
        let isEmptyNextTopItem = isNotEdge && checkEmptyNextTopItem(field, position[0], position[1] - 1);
        let isEmptyNextBottomItem = isNotEdge && checkEmptyNextBottomItem(field, position[0], position[1] - 1);

        return isEmptyLeftItem && isEmptyNextLeftItem && isEmptyNextTopItem && isEmptyNextBottomItem;
    }


    /**
     * Проверка можно ли построить дорожку вправо
     *
     * @param {Array} field Игровое поле
     * @param {Array} position Текущая позиция в виде массива [y, x]
     *
     * @return {boolean}
     */
    function isCanCreateDirectionToRight(field, position) {
        let isNotEdge = false === checkRightOrBottomEdge(position[1], field.length);
        let isEmptyRightItem = checkEmptyRightItem(field, position[0], position[1]);
        let isEmptyNextRightItem = checkEmptyNextRightItem(field, position[0], position[1] + 1);
        let isEmptyNextTopItem = checkEmptyNextTopItem(field, position[0], position[1] + 1);
        let isEmptyNextBottomItem = checkEmptyNextBottomItem(field, position[0], position[1] + 1);

        return isNotEdge && isEmptyRightItem && isEmptyNextRightItem && isEmptyNextTopItem && isEmptyNextBottomItem;
    }


    /**
     * Получение направления для построения отрезка дорожки
     *
     * @param {Array} directions Список направлений
     *                                   [0] - направление вверх
     *                                   [1] - направление вниз
     *                                   [2] - направление влево
     *                                   [3] - направление вправо
     * @param {Array} field Игровое поле
     * @param {Array} position Стартовая позиция отрезка
     * @param {number} [predDirectionRoad] предыдущее направление
     *
     * @return {number|boolean} Возвращает номер направления, если отрезок строить некуда, то false
     */
    function getDirectionRoad(directions, field, position, predDirectionRoad) {
        let removeDirections = [];

        if (false === isCanCreateDirectionToTop(field, position)) {
            removeDirections.push(0)
        }

        if (false === isCanCreateDirectionToBottom(field, position)) {
            removeDirections.push(1)
        }

        if (false === isCanCreateDirectionToLeft(field, position)) {
            removeDirections.push(2)
        }

        if (false === isCanCreateDirectionToRight(field, position)) {
            removeDirections.push(3)
        }


        directions = directions.filter(function (direction) {
            return removeDirections.indexOf(direction) === -1 && (typeof predDirectionRoad === "undefined" || (directions.length > 1 && direction !== predDirectionRoad));
        });

        return directions.length > 0 ? directions[getRandomNumber(directions.length)] : false;
    }

    /**
     * Получение количество шагов для отрезка дорожки по заданному направлению
     *
     * @param {number|boolean} directionRoad Номер направления отрезка
     * @param {Array} position Стартовая позиция отрезка виде массива [y,x]
     * @param {number} sizeGameFiled Размер игрового поля
     *
     * @return {number|boolean} Возвращает количество шагов, если направления нет, то false
     */
    function getNumSteps(directionRoad, position, sizeGameFiled) {
        let numSteps = null;
        switch (directionRoad) {
            case 0:
                numSteps = getRandomNumber(position[0]);
                break;
            case 1:
                numSteps = getRandomNumber(sizeGameFiled - position[0]);
                break;
            case 2:
                numSteps = getRandomNumber(position[1]);
                break;
            case 3:
                numSteps = getRandomNumber((sizeGameFiled - position[1]));
                break;
            default:
                numSteps = false;
                break;
        }

        numSteps = numSteps !== false ? (numSteps > 0 ? numSteps : 1) : false;

        // если количество шагов в ходу больше чем половина размера поля, то уменьшаем количество шагов на эту половину,
        // чтобы увеличить количество разломов и уменьше длину прямых отрезков
        return numSteps !== false ? Math.floor(numSteps < sizeGameFiled / 2 ? numSteps : numSteps - sizeGameFiled / 2) : false;
    }


    /**
     * Генерация дорожки и направлений
     *
     * @param {Array} field массив игрового поля
     * @param {number} sizeGameFiled размер игрового поля
     * @param {Array} position последняя позиция постороеной дорожки
     * @param {number} turn оставщееся максимальное число поворотов
     * @param {number} predDirectionRoad предыдущее направление движения
     * @param {Array} [directionsPrev] массив направлений из предыдущей итерации, передаётся только в том случае,
     *                               когда не удалось построить отрезок дорожки в предыдущей итерации
     *
     * @returns {Array}
     */
    function generateRoad(field, sizeGameFiled, position, turn, predDirectionRoad, directionsPrev) {

        /**
         * Список направлений движения
         *
         * @type {Array}
         * @property [0] - движение вверх
         * @property [1] - движение вниз
         * @property [2] - движение влево
         * @property [3] - движение вправо
         */
        let directions = typeof directionsPrev === "undefined" ? [0, 1, 2, 3] : directionsPrev;

        // Если оставщееся максимальное число поворотов равно 0 или список направлений пуст, то выходим из рекурсии
        if (turn > 0 && directions.length > 0) {

            let directionRoad = getDirectionRoad(directions, field, position, predDirectionRoad);
            let numSteps = getNumSteps(directionRoad, position, sizeGameFiled);
            let newfieldData = numSteps !== false ? fillDirectionRoad(field, position, directionRoad, numSteps) : false;
            console.log(numSteps);
            // Если направлений для дальнейшего построения нет, то обнуляем оставшееся количество поворотов
            turn = directionRoad === false ? 0 : turn;

            return newfieldData !== false
                // если отрезок сгенерирован, то запускаем генерацию с обновленными значениями
                ? generateRoad(newfieldData.field, sizeGameFiled, newfieldData.position, --turn, directionRoad)
                // если в текущей итерации не удалось построить отрезок,
                // то запускаем генерацию ещё раз убрав текущее направление
                : generateRoad(field, sizeGameFiled, position, turn, directionRoad, directions.filter(function (direction) {
                    return direction !== directionRoad;
                }));
        } else if (turn <= 0 && (getDirectionRoad(directions, field, position) !== false)) {
            return generateRoad(field, sizeGameFiled, position, (getRandomNumber(sizeGameFiled + 5) + getRandomNumber(sizeGameFiled + 5)), predDirectionRoad)
        }

        // если выходим из рекурсии, то на последней точке ставим финиш
        field[position[0]][position[1]] = 3;
        return field;
    }

    /**
     * Генерация стартового элемента и максимального числа поворотов
     *
     * @param {Array} field Игровое поле
     * @param {number} sizeGameFiled Размер поля
     * @returns {Array} Игровое поле со сгенерированной дорожкой
     */
    function generateMap(field, sizeGameFiled) {

        let randomStartWall = getRandomNumber(2) === 0 ? 0 : (sizeGameFiled - 1);
        startPos = getRandomNumber(2) === 0 ? [randomStartWall, getRandomNumber(sizeGameFiled)] : [getRandomNumber(sizeGameFiled), randomStartWall];
        field[startPos[0]][startPos[1]] = 2;

        let turns = getRandomNumber(sizeGameFiled + 5) + getRandomNumber(sizeGameFiled + 5);

        return generateRoad(field, sizeGameFiled, startPos, turns);
    }


    /**
     * Генерация игрового поля и заполенение нулями.
     * Размер поля выбирается случайным значением между установленными минимальным и максимальным.
     *
     */
    function generateGameField() {
        let sizeGameFiled = getRandomNumber(maxSizeField, minSizeField);

        for (let y = 0; y < sizeGameFiled; y++) {
            gameField[y] = [];
            for (let x = 0; x < sizeGameFiled; x++) {
                gameField[y][x] = 0;
            }
        }

        gameField = generateMap(gameField, sizeGameFiled);

        fillGameField(gameField);
    }


    /**
     * Отрисовка и заполнение игровых ячеек
     *
     * @param gameField
     */
    function fillGameField(gameField) {
        let widthGameFiled = gameField.length * sizeFieldItem;
        gameFieldBlock.css({minWidth: widthGameFiled + 'px', maxWidth: widthGameFiled + 'px'});

        gameField.forEach(function (itemY, y) {
            itemY.forEach(function (itemX, x) {
                let item = $('<div></div>').addClass('game_field-item').attr('data-y', y).attr('data-x', x).height(sizeFieldItem).width(sizeFieldItem);

                switch (itemX) {
                    case 1:
                        item.addClass('road');
                        break;
                    case 2:
                        item.addClass('start');
                        startPos = [y, x];
                        break;
                    case 3:
                        item.addClass('finish');
                        break;
                }

                gameFieldBlock.append(item);
            });
        });

    }

    function addChip() {
        let newChip = $('<div></div>')
            .addClass('chip')
            .attr('data-player', players.length)
            .data('player', players.length)
            .css('background', 'rgba(' + (players.length + getRandomNumber(150, 20)) + ',' + (players.length + getRandomNumber(150, 20)) + ',' + (players.length + getRandomNumber(150, 20)) + ')')
            .height(sizeFieldItem - (marginChip * 2))
            .width(sizeFieldItem - (marginChip * 2))
            .text(players.length)
        ;
        gameFieldBlock.append(newChip);

        gameFieldBlock.find('.chip').css('z-index', 1);
        gameFieldBlock.find('.chip[data-player="' + players.length + '"]').css('z-index', 10);
        setPositionChip(players.length, startPos);
    }

    function setPositionChip(player, position) {
        prevPos[player] = getPositionChip(player);
        gameFieldBlock.find('.chip[data-player="' + player + '"]')
            .css({top: position[0] * sizeFieldItem + marginChip, left: position[1] * sizeFieldItem + marginChip})
            .attr('data-y', position[0])
            .attr('data-x', position[1])
            .data('y', position[0])
            .data('x', position[1]);
    }

    function getPositionChip(player) {
        let chip = gameFieldBlock.find('.chip[data-player="' + player + '"]');
        return [chip.data('y'), chip.data('x')];
    }


    function startGame() {
        resetParameters();
        generateGameField();
        addChip();
        enableControls();
    }

    function enableControls() {
        btnAddPlayer.prop('disabled', false);
        btnMakeMovePlayer.prop('disabled', false);
        gameFieldBlock.css({visibility: 'visible'});
    }

    function resetParameters() {
        gameFieldBlock.css({visibility: 'hidden'}).empty();
        blockResult.empty();
        players = [1];
        finishedGame = [];
        currentPlayer = 1;
        startPos = [];
        gameField = [];
        prevPos = [];
        fieldNumSteps.val('');
    }


    /**
     * Конец хода
     *
     * @param currentPosition
     * @param indexCurrentPlayer
     */
    function movementEnd(currentPosition, indexCurrentPlayer) {
        if (gameField[currentPosition[0]][currentPosition[1]] === 3) {
            playerComleted();
        }

        moveComlete(indexCurrentPlayer);
    }

    /**
     * Перемещение на 1 шаг
     *
     * @param {Array} currentPosition
     * @param {number} indexCurrentPlayer
     * @param {number} step
     *
     * @return {number}
     */
    function stepMovement(currentPosition, indexCurrentPlayer, step) {

        if (gameField[currentPosition[0]][currentPosition[1]] === 3) {
            playerComleted();
            moveComlete(indexCurrentPlayer, loopSteps);
        } else {
            setPositionChip(currentPlayer, getNewPositions(currentPosition, currentPlayer));
            step++;
        }

        return step;
    }


    function moveComlete(indexCurrentPlayer) {
        btnMakeMovePlayer.prop('disabled', false);
        clearInterval(loopSteps);

        if (players.length > 0) {
            setNextPlayer(indexCurrentPlayer);
            btnMakeMovePlayer.text('Ход игрока ' + currentPlayer);
        } else {
            endGame();
        }

    }

    function playerComleted() {
        finishedGame.push(currentPlayer);
        players = players.filter(function (item) {
            return item !== currentPlayer;
        });
        viewResult();
    }

    function setNextPlayer(index) {
        if (players.length > 1 && players.indexOf(currentPlayer) === -1 && index < players.length) {
            currentPlayer = players[index];
        } else if (players.length > 1 && players.indexOf(currentPlayer) !== -1 && index < players.length - 1) {
            currentPlayer = players[index + 1];
        } else {
            currentPlayer = players[0];
        }

        gameFieldBlock.find('.chip').css('z-index', 1);
        gameFieldBlock.append(gameFieldBlock.find('.chip[data-player="' + currentPlayer + '"]').css('z-index', 10));
    }

    function endGame() {
        btnMakeMovePlayer.text('Игра окончена').prop('disabled', true);
    }

    function viewResult() {
        blockResult.empty();
        finishedGame.forEach(function (player, index) {
            blockResult.append('<tr><th>' + (index + 1) + '</th><th>' + player + '</th></tr>');
        })
    }

    /**
     * Получения новых координат для хода
     *
     * @param {Array} position
     * @param {number} player
     *
     * @return {Array} новые координаты
     */
    function getNewPositions(position, player) {
        let newPos = position;

        if (checkMoveRigth(position[1], position[0], player)) {
            newPos[1]++;
        } else if (checkMoveLeft(position[1], position[0], player)) {
            newPos[1]--;
        } else if (checkMoveBottom(position[1], position[0], player)) {
            newPos[0]++;
        } else if (checkMoveTop(position[1], position[0], player)) {
            newPos[0]--;
        }

        return newPos;
    }

    /**
     * Проверяет доступность для хода влево
     *
     * @param {number} posX
     * @param {number} posY
     * @param {number} player
     *
     * @return {boolean}
     */
    function checkMoveLeft(posX, posY, player) {
        return posX > 0 && prevPos[player][1] !== posX - 1 && gameField[posY][posX - 1] !== 0;
    }

    /**
     * Проверяет доступность для хода вправо
     *
     * @param {number} posX
     * @param {number} posY
     * @param {number} player
     *
     * @return {boolean}
     */
    function checkMoveRigth(posX, posY, player) {
        return posX < gameField.length - 1 && prevPos[player][1] !== posX + 1 && gameField[posY][posX + 1] !== 0;
    }

    /**
     * Проверяет доступность для хода вниз
     *
     * @param {number} posX
     * @param {number} posY
     * @param {number} player
     *
     * @return {boolean}
     */
    function checkMoveBottom(posX, posY, player) {
        return posY < gameField.length - 1 && prevPos[player][0] !== posY + 1 && gameField[posY + 1][posX] !== 0;
    }

    /**
     * Проверяет доступность для хода вверх
     *
     * @param {number} posX
     * @param {number} posY
     * @param {number} player
     *
     * @return {boolean}
     */
    function checkMoveTop(posX, posY, player) {
        return posY > 0 && prevPos[player][0] !== posY - 1 && gameField[posY - 1][posX] !== 0;
    }


    btnStartGame.on('click', startGame);

    btnAddPlayer.on('click', function () {
        players.push(players.length + 1);
        addChip();
    });

    btnMakeMovePlayer.on('click', function () {


        btnAddPlayer.prop('disabled', true); // отключаем кнопку добавления фишек
        btnMakeMovePlayer.prop('disabled', true); // отключаем кнопку хода

        let numSteps = cube[getRandomNumber(cube.length)]; // генерируем количество шагов для хода
        fieldNumSteps.val(numSteps);

        let numStep = 0;

        loopSteps = setInterval(function () {
            let currentPosition = getPositionChip(currentPlayer); // получение позиции активной фишки
            let indexCurrentPlayer = players.indexOf(currentPlayer); // получение индекса активной фишки в массиве игроков

            if (numStep < numSteps) {
                numStep = stepMovement(currentPosition, indexCurrentPlayer, numStep);
            } else {
                movementEnd(currentPosition, indexCurrentPlayer);
            }

        }, 300);
    });


    /**
     * Генерация случайно числа от 0 до max или от min до max
     *
     * @param {number} max максимальное значение
     * @param {number} [min] минимальное значение
     *
     * @return {number}
     */
    function getRandomNumber(max, min) {
        return typeof min === "undefined" || isNaN(parseInt(min.toString())) || min === 0 ? Math.floor(Math.random() * max) : Math.floor(Math.random() * (max - min + 1) + min);
    }
});